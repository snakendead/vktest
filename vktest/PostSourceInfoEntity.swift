//
//  PostSourceInfoEntity.swift
//  vktest
//
//  Created by developer2 on 13.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import Foundation
import CoreData


class PostSourceInfoEntity: NSManagedObject {

    var type: String?
    var platform: String?
    var data: String?
    var uri: String?

}
