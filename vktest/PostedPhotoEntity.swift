//
//  PostedPhotoEntity.swift
//  vktest
//
//  Created by developer2 on 13.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import Foundation
import CoreData


class PostedPhotoEntity: NSManagedObject {

    var id: NSNumber?
    var owner_id: NSNumber?
    var photo_130: String?
    var photo_604: String?

}
