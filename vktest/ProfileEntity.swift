//
//  ProfileEntity.swift
//  vktest
//
//  Created by developer2 on 17.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import Foundation
import CoreData


class ProfileEntity: NSManagedObject {

    var id: NSNumber?
    var first_name: String?
    var last_name: String?
    var sex: NSNumber?
    var screen_name: String?
    var photo_50: String?
    var photo_100: String?
    var photo_200: String?
    var online: NSNumber?

}
