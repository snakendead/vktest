//
//  GroupEntity.swift
//  vktest
//
//  Created by developer2 on 17.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import Foundation
import CoreData


class GroupEntity: NSManagedObject {

    var id: NSNumber?
    var name: String?
    var screen_name: String?
    var is_closed: NSNumber?
    var is_admin: NSNumber?
    var is_member: NSNumber?
    var type: String?
    var photo_50: String?
    var photo_100: String?
    var photo_200: String?

}
