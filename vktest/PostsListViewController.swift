//
//  PostsListViewController.swift
//  vktest
//
//  Created by developer2 on 12.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit
import MagicalRecord

class PostsListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let cellIdentifier = "FeedCell"
    let pageCount = 20
    var loading = false
    
    var data: [WallItemEntity] = []
    var profiles: [Int32: ProfileEntity] = [:]
    var groups: [Int32: GroupEntity] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        tableView.tableHeaderView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 411.0
        let nibCell = UINib(nibName: "FeedCell", bundle: nil)
        tableView.registerNib(nibCell, forCellReuseIdentifier: cellIdentifier)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        fillPage()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func fillPage() {
        guard loading == false else { return }
        loading = true
        WallItemEntity.getSelfWall(pageCount, offset: data.count) {
            (data, profiles, groups, error, count) in
            self.data.appendContentsOf(data)
            
            for profile in profiles {
                self.profiles[profile.id!.intValue] = profile
            }
            for group in groups {
                self.groups[group.id!.intValue] = group
            }
            self.tableView.reloadData()
            guard self.data.count < count else { return }
            self.loading = false
        }
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

}

extension PostsListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! FeedCell
        let entity = data[indexPath.row]
        fillCell(cell, withEntity: entity)
        
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row > self.data.count - 3 { fillPage() }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func fillCell(cell: FeedCell, withEntity entity: WallItemEntity) {
        
        let historyPost = entity.copy_history
        
        // Start with text of main post, and if we have a copy_history - will ad this text to test.
        cell.label.text = entity.text
        if historyPost != nil {
            if cell.label.text! != "" {
                cell.label.text = cell.label.text! + "\n"
                cell.label.text = cell.label.text! + "*** *** *** *** ***"
                cell.label.text = cell.label.text! + "\n"
            }
            cell.label.text = cell.label.text! + historyPost!.text!
        }

        // Fill a first header, with history info
        
        var historyHeaderName: String = ""
        var historyHeaderStatus: String = ""
        var historyHeaderDate: String = ""
        var historyHeaderTime: String = ""
        var historyHeaderUrl: String = ""

        var headerName: String = ""
        var headerStatus: String = ""
        var headerDate: String = ""
        var headerTime: String = ""
        var headerUrl: String = ""

        var attachments = [AttachmentsInfoEntity]()

        if let attachmentsSet = entity.attachments {
            for obj in attachmentsSet {
                attachments.append(obj as! AttachmentsInfoEntity)
            }
        }

        if historyPost != nil {
            let postFromId = entity.from_id == nil ? 0 : entity.from_id!.intValue
            let historyFromId = historyPost!.from_id == nil ? 0 : historyPost!.from_id!.intValue

            if postFromId < 0 {
                let group = groups[entity.from_id!.intValue * -1]!
                historyHeaderName = safeString(group.name)
                historyHeaderStatus = safeString(group.screen_name)
                historyHeaderUrl = safeString(group.photo_100)
            } else {
                let profile = profiles[entity.from_id!.intValue]!
                historyHeaderName = safeString(profile.first_name) + " " + safeString(profile.last_name)
                historyHeaderStatus = safeString(profile.screen_name)
                historyHeaderUrl = safeString(profile.photo_100)
            }
            historyHeaderDate = dateFromDate(entity.date?.doubleValue)
            historyHeaderTime = timeFromDate(entity.date?.doubleValue)
            
            
            if historyFromId < 0 {
                let group = groups[historyPost!.from_id!.intValue * -1]!
                headerName = safeString(group.name)
                headerStatus = safeString(group.screen_name)
                headerUrl = safeString(group.photo_100)
            } else {
                let profile = profiles[historyPost!.from_id!.intValue]!
                headerName = safeString(profile.first_name) + " " + safeString(profile.last_name)
                headerStatus = safeString(profile.screen_name)
                headerUrl = safeString(profile.photo_100)
            }
            headerDate = dateFromDate(entity.date?.doubleValue)
            headerTime = timeFromDate(entity.date?.doubleValue)
            
            if let attachmentsSet = historyPost!.attachments {
                for obj in attachmentsSet {
                    attachments.append(obj as! AttachmentsInfoEntity)
                }
            }

        } else {
            let postFromId = entity.from_id == nil ? 0 : entity.from_id!.intValue
            if postFromId < 0 {
                let group = groups[postFromId]!
                headerName = safeString(group.name)
                headerStatus = safeString(group.screen_name)
                headerUrl = safeString(group.photo_100)
            } else {
                let profile = profiles[postFromId]!
                headerName = safeString(profile.first_name) + " " + safeString(profile.last_name)
                headerStatus = safeString(profile.screen_name)
                headerUrl = safeString(profile.photo_100)
            }
            headerDate = dateFromDate(entity.date?.doubleValue)
            headerTime = timeFromDate(entity.date?.doubleValue)
        }
        
        cell.headerHeightConstraint.constant = entity.copy_history == nil ? 0 : 90

        if let url = NSURL(string: historyHeaderUrl) {
            cell.headerImageURL = url
            if let image = url.cachedImage {
                cell.headerProfileImage.image = image
            } else {
                url.fetchImage { image in
                    if cell.headerImageURL == url {
                        cell.headerProfileImage.image = image
                    }
                }
            }
        }

        if let url = NSURL(string: headerUrl) {
            cell.imageURL = url
            if let image = url.cachedImage {
                cell.profileImage.image = image
            } else {
                url.fetchImage { image in
                    if cell.imageURL == url {
                        cell.profileImage.image = image
                    }
                }
            }
        }

        cell.nameLabel.text = headerName
        cell.headerNameLabel.text = historyHeaderName
        
        cell.statusLabel.text = headerStatus
        cell.headerStatusLabel.text = historyHeaderStatus
        
        cell.dateLabel.text = headerDate
        cell.headerDateLabel.text = historyHeaderDate
        
        cell.timeLabel.text = headerTime
        cell.headerTimeLabel.text = historyHeaderTime
        
        cell.likessCountLabel.text = entity.likes?.count?.stringValue
        cell.repostsCountLabel.text = entity.reposts?.count?.stringValue
        
        for subview in cell.photoParentView.subviews {
            subview.removeFromSuperview()
        }
        
        if attachments.count != 0 {
            var temp: CGFloat = 0
            let xOffset: CGFloat = 100.0
            for attachment in attachments {
                guard attachment.type! == "photo" else { continue }
                let imageView = PhotoImageView(frame: CGRectMake(10.0 + xOffset*temp, 10.0, 100.0, 80.0))
                cell.photoParentView.addSubview(imageView)
                var urlString = ""
                if attachment.photo?.photo_604 != nil {
                    urlString = attachment.photo!.photo_604!
                } else if attachment.photo?.photo_130 != nil {
                    urlString = attachment.photo!.photo_130!
                }
                if let url = NSURL(string: urlString) {
                    if let image = url.cachedImage {
                        imageView.image = image
                    } else {
                        url.fetchImage { image in
                            imageView.image = image
                        }
                    }
                }
                
                temp += 1
            }
        }
        
        cell.photoParentViewHeightConstraint.constant = cell.photoParentView.subviews.count == 0 ?0.0 : 100.0
    }
}
