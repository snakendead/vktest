//
//  LoginButton.swift
//  vktest
//
//  Created by developer2 on 13.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit

@IBDesignable

class LoginButton: UIButton {
    
    override func prepareForInterfaceBuilder() {
        baseInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        baseInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        baseInit()
    }

    func baseInit() {
        layer.cornerRadius = 5.0
        layer.borderWidth = 2.0
        layer.masksToBounds = true
        checkState()
    }
    
    override func drawRect(rect: CGRect) {
        checkState()
    }
    
    func checkState() {
        layer.borderColor = titleColorForState(state)?.CGColor
    }
    
    override var highlighted: Bool {
        set {
            
        }
        get {
            return false
        }
    }
}
