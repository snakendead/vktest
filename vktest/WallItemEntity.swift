//
//  WallItemEntity.swift
//  vktest
//
//  Created by developer2 on 13.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

class WallItemEntity: NSManagedObject {
    
    var can_delete: NSNumber?
    var can_edit: NSNumber?
    var can_pin: NSNumber?
    var date: NSNumber?
    var friends_only: NSNumber?
    var from_id: NSNumber?
    var id: NSNumber?
    var is_pinned: NSNumber?
    var owner_id: NSNumber?
    var post_type: String?
    var reply_owner_id: NSNumber?
    var reply_post_id: NSNumber?
    var signer_id: NSNumber?
    var text: String?
    var attachments: NSSet?
    var comments: CommentsInfoEntity?
    var copy_history: CopyHistoryEntity?
    var likes: LikesInfoEntity?
    var post_source: PostSourceInfoEntity?
    var reposts: RepostsInfoEntity?

    class func getSelfWall(count: Int, offset: Int, handler: ((data: [WallItemEntity], profiles: [ProfileEntity], groups: [GroupEntity] , error: NSError?, count: Int) -> ())) {
        let keychainWrapper = KeychainWrapper()
        DataProvider.requestProducer(DataProvider.vkMethod.WallGet,
                                     params: ["filter":"all",
                                        "count": count,
                                        "offset": offset,
                                        "access_token": keychainWrapper.myObjectForKey(kSecValueData),
                                        "extended": 1,
                                        "v": 5.53],
                                     actionType: DataProvider.ActionType.Get) { (data, error) in
                                        guard error == nil else { handler(data: [], profiles:[], groups: [], error: error!, count: 0); return }
                                        var responseDict = data?.dictionary!["response"]?.dictionary
                                        let count = responseDict!["count"]!.intValue
                                        
                                        let items = responseDict!["items"]!.arrayValue
                                        let profiles = responseDict!["profiles"]!.arrayValue
                                        let groups = responseDict!["groups"]!.arrayValue
                                        
                                        let wallsEntities = items.map({ (obj) -> WallItemEntity in
                                            let entity = WallItemEntity.MR_createEntity()!
                                            entity.safeSetValuesForKeysWithDictionary(obj.dictionaryObject!)
                                            return entity
                                        })

                                        let profilesEntities = profiles.map({ (obj) -> ProfileEntity in
                                            let entity = ProfileEntity.MR_createEntity()!
                                            entity.safeSetValuesForKeysWithDictionary(obj.dictionaryObject!)
                                            return entity
                                        })

                                        let groupsEntities = groups.map({ (obj) -> GroupEntity in
                                            let entity = GroupEntity.MR_createEntity()!
                                            entity.safeSetValuesForKeysWithDictionary(obj.dictionaryObject!)
                                            return entity
                                        })

                                        handler(data: wallsEntities, profiles: profilesEntities, groups: groupsEntities, error: nil, count: count)
        }
    }
}
