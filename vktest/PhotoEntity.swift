//
//  PhotoEntity.swift
//  vktest
//
//  Created by developer2 on 13.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import Foundation
import CoreData


class PhotoEntity: NSManagedObject {

    var album_id: NSNumber?
    var date: NSNumber?
    var height: NSNumber?
    var id: NSNumber?
    var owner_id: NSNumber?
    var photo_75: String?
    var photo_130: String?
    var photo_604: String?
    var photo_807: String?
    var photo_1280: String?
    var photo_2560: String?
    var text: String?
    var user_id: NSNumber?
    var width: NSNumber?

}
