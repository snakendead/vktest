//
//  LoginViewController.swift
//  vktest
//
//  Created by developer2 on 12.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit
import SafariServices

class LoginViewController: UIViewController {

    @IBOutlet weak var loginButton: LoginButton!
    @IBOutlet weak var feedButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // Programmarically setup VC
    
    private func setupView() {
        checkToken()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

    }
    
    // IBActions
    
    @IBAction func loginButtonAction(sender: LoginButton) {
        sender.selected ? logoutAction() : loginAction()
    }
    
    func logoutAction() {
        let keychain = KeychainWrapper()
        keychain.mySetObject("", forKey: kSecValueData)
        keychain.writeToKeychain()
        removeCookiesDomain("vk.com")
        checkToken()
    }
    
    func loginAction() {
        let url = NSURL(string: "https://oauth.vk.com/authorize?client_id=4918712&groups_ids=1&v=5.3&display=mobile&redirect_uri=http://google.com/callback&scope=friends,wall&response_type=token")
        let webView = UIWebView(frame: CGRectOffset(view.bounds, 0, 20))
        webView.frame.size.height -= 20
        webView.delegate = self
        webView.loadRequest(NSURLRequest(URL: url!))
        UIView.transitionWithView(self.view, duration: 0.3, options: UIViewAnimationOptions.CurveEaseIn, animations: { 
            self.view.addSubview(webView)
        }) { (finish) in
                
        }
    }

    func checkToken() {
        // check access_token for VK API
        let keychain = KeychainWrapper()
        let accessToken = keychain.myObjectForKey(kSecValueData) as! String
        keychain.writeToKeychain()
        loginButton.selected = accessToken != ""
        feedButton.enabled = loginButton.selected
    }
}

extension LoginViewController: UIWebViewDelegate {
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        return allowWebViewWithRequest(request, webView: webView)
    }
    
    func allowWebViewWithRequest(request: NSURLRequest, webView: UIWebView) -> Bool {
        let urlString = request.URL!.absoluteString
        if urlString.containsString("vk.com") { return true }
        guard urlString.containsString("google.com") else { return false }
        defer {
            UIView.transitionWithView(self.view, duration: 0.3, options: UIViewAnimationOptions.CurveEaseIn, animations: {
                webView.removeFromSuperview()
            }) { (finish) in
                guard finish == true else { return }
                self.checkToken()
            }
        }

        let urlComps = NSURLComponents(string: urlString.stringByReplacingOccurrencesOfString("callback#", withString: "?"))
        var token = ""
        for queryItem in urlComps!.queryItems! {
            if queryItem.name == "access_token" { token = queryItem.value!; break }
        }
        guard token != "" else { return true }
        let keychain = KeychainWrapper()
        keychain.mySetObject(token, forKey: kSecValueData)
        keychain.writeToKeychain()

        return true
    }
}