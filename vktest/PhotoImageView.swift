//
//  PhotoImageView.swift
//  vktest
//
//  Created by developer2 on 17.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit

class PhotoImageView: UIImageView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    func initialize() {
        contentMode = UIViewContentMode.ScaleAspectFill
        layer.masksToBounds = true
//        layer.shadowColor = UIColor.blackColor().CGColor
//        layer.shadowOffset = CGSizeMake(-3.0, 2.0)
//        layer.shadowRadius = 2.0
//        layer.shadowOpacity = 0.4
    }
    
}
