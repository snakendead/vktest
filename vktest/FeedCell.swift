//
//  FeedCell.swift
//  vktest
//
//  Created by developer2 on 14.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit

class FeedCell: UITableViewCell {

    @IBOutlet weak var headerProfileImage: UIImageView!
    @IBOutlet weak var headerNameLabel: UILabel!
    @IBOutlet weak var headerStatusLabel: UILabel!
    @IBOutlet weak var headerDateLabel: UILabel!
    @IBOutlet weak var headerTimeLabel: UILabel!
    
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var photoParentView: UIView!
    
    @IBOutlet weak var photoParentViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var likessCountLabel: UILabel!
    @IBOutlet weak var repostsCountLabel: UILabel!
    
    var headerImageURL: NSURL!
    var imageURL: NSURL!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
