//
//  CopyHistoryEntity.swift
//  vktest
//
//  Created by developer2 on 17.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import Foundation
import CoreData


class CopyHistoryEntity: NSManagedObject {

    var date: NSNumber?
    var from_id: NSNumber?
    var id: NSNumber?
    var owner_id: NSNumber?
    var post_type: String?
    var signer_id: NSNumber?
    var text: String?
    var attachments: NSSet?
    var post_source: PostSourceInfoEntity?

}
