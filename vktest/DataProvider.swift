//
//  DataProvider.swift
//  vktest
//
//  Created by developer2 on 12.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit
import MagicalRecord
import Alamofire
import SwiftyJSON

class DataProvider: NSObject {
    
    static let baseUrl = "https://api.vk.com/method/"

    enum vkMethod: String {
        case WallGet = "wall.get"
    }

    enum APIError: ErrorType {
        case URLFailed
        case RequestFailed
        case RequestError
        case SerializationFailed
    }
    
    enum ActionType: String {
        case Get = "GET"
        case Post = "POST"
        case Put = "PUT"
        case Delete = "DELETE"
    }
    
    class func requestProducer(method: vkMethod,
                        params: [String: AnyObject]?,
                         actionType: ActionType,
                         codes: [Int] = [],
                         completitionHandler: ((data: JSON?, error: NSError?) -> ())) {
        
        var successCodes = [200, 201, 202]
        successCodes.appendContentsOf(codes)
        let urlString = baseUrl + method.rawValue
        guard let url = NSURL(string: urlString) else {
            let error = NSError(domain: "Ошибка URL", code: 1001, userInfo: nil)
            completitionHandler(data: nil, error: error)
            return
        }
        var headers = [String: String]()
        var encoding: ParameterEncoding = ParameterEncoding.URLEncodedInURL
        if actionType != ActionType.Get {
            if params != nil {
                headers["Content-Type"] = "application/json"
                encoding = ParameterEncoding.JSON
            }
        }

        let method = [ActionType.Get: Alamofire.Method.GET,
            ActionType.Post: Alamofire.Method.POST,
            ActionType.Put: Alamofire.Method.PUT,
            ActionType.Delete: Alamofire.Method.DELETE]
        
        let request = Alamofire.request(method[actionType]!,
            url,
            parameters: params,
            encoding: encoding,
            headers: headers)
        
        request
            .validate({ (request, response) -> Request.ValidationResult in
                let status = response.statusCode
                guard !successCodes.contains(status) else { return Request.ValidationResult.Success }
                var errorDomain = ""
                switch status {
                case 0..<300:
                    return Request.ValidationResult.Success
                case 400:
                    errorDomain = "Плохой, неверный запрос"
                case 401:
                    errorDomain = "Не авторизован"
                case 403:
                    errorDomain = "Протухший токен"
                case 405:
                    errorDomain = "Метод не поддерживается"
                case 409:
                    errorDomain = "Конфликт"
                case 412:
                    errorDomain = "Условие ложно"
                case 413:
                    errorDomain = "Размер запроса слишком велик"
                case 501:
                    errorDomain = "Не реализовано"
                case 503:
                    errorDomain = "Сервис недоступен"
                case 300..<500:
                    errorDomain = "Неверный запрос"
                case 500..<600:
                    errorDomain = "Ошибка сервера"
                default:
                    errorDomain = "Неизвестная ошибка"
                }
                
                return Request.ValidationResult.Failure(NSError(domain: errorDomain, code: status, userInfo: nil))
            })
            .responseString(completionHandler: { response in
                switch response.result {
                case .Success(let responseObject):
                    let data = responseObject.dataUsingEncoding(NSUnicodeStringEncoding)
                    
                    var dict: AnyObject? = nil
                    
                    do {
                        dict = try NSJSONSerialization.JSONObjectWithData(data!, options: [NSJSONReadingOptions.AllowFragments, NSJSONReadingOptions.MutableLeaves, NSJSONReadingOptions.MutableContainers])
                    } catch {
                        let error = NSError(domain: "Парсинг облажал", code: 0, userInfo: nil)
                        showError(error)
                        completitionHandler(data: nil, error: error)
                        return
                    }
                    
                    let jsonDict = dict != nil ? JSON(dict!) : JSON(responseObject)
                    if jsonDict != nil {
                        if jsonDict["error"].dictionary != nil {
                            let error = NSError(domain: jsonDict["error"].stringValue, code: 1000, userInfo: nil)
                            completitionHandler(data: nil, error: error)
                            return
                        }
                    }
                    
                    completitionHandler(data: jsonDict, error: nil)
                    return

                case .Failure(let error):
                    showError(error)
                    completitionHandler(data: nil, error: error)
                }
            })
    }
}

func showError(error: NSError) {
    guard let topNotNil = UIApplication.topViewController() else { return }
    let alert = UIAlertController(title: "Ошибка \(error.code)", message: error.domain, preferredStyle: UIAlertControllerStyle.Alert)
    let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil)
    alert.addAction(action)
    topNotNil.presentViewController(alert, animated: true, completion: nil)
}

extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.sharedApplication().keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
}

extension NSManagedObject {
    func safeSetValuesForKeysWithDictionary(keyedValues: [String: AnyObject]) {
        let attributes = self.entity.attributesByName
        
        for (key, attribute) in attributes {
            var newValue: AnyObject? = keyedValues[key]
            guard key != "descript" else { self.setValue(keyedValues["description"], forKey: key); continue }
            guard newValue != nil else { continue }
            let attributeType = attribute.attributeType
            switch attributeType {
            case .StringAttributeType:
                if newValue! is NSNumber { newValue = newValue!.stringValue }
            case .BooleanAttributeType:
                break
            case .Integer64AttributeType:
                break
            case .FloatAttributeType:
                break
            case .ObjectIDAttributeType:
                break
            default: break
            }
            guard !newValue!.isEqual(NSNull()) else { continue }
            
            self.setValue(newValue, forKey: key)
        }
        
        let relationships = self.entity.relationshipsByName
        for (key, relationship) in relationships {
            var newValue: AnyObject? = keyedValues[key]
            guard newValue != nil else { continue }
            guard !newValue!.isKindOfClass(NSNull) else { continue }

            if relationship.toMany {
                var set: Set<NSManagedObject> = []
                let array = newValue as! [[String: AnyObject]]
                for item in array {
                    let map = ["attachments": AttachmentsInfoEntity.MR_createEntity()!]
                    let object = map[key]!
                    object.safeSetValuesForKeysWithDictionary(item)
                    set.insert(object)
                }
                newValue = set
            } else {
                let map = ["comments": CommentsInfoEntity.MR_createEntity()!,
                           "likes": LikesInfoEntity.MR_createEntity()!,
                           "photo": PhotoEntity.MR_createEntity()!,
                           "post_source": PostSourceInfoEntity.MR_createEntity()!,
                           "posted_photo": PostedPhotoEntity.MR_createEntity()!,
                           "reposts": RepostsInfoEntity.MR_createEntity()!,
                           "copy_history": CopyHistoryEntity.MR_createEntity()!]
                
                if map[key] != nil {
                    let object = map[key]!
                    object.safeSetValuesForKeysWithDictionary(key != "copy_history" ? newValue as! [String: AnyObject] : (newValue as! [[String: AnyObject]]).first!)
                    newValue = object
                }
            }

            self.setValue(newValue, forKey: key)
        }
    }
}