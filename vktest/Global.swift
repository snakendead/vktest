//
//  Global.swift
//  vktest
//
//  Created by developer2 on 13.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit

func safeString(string: String?) -> String {
    return string == nil ? "" : string!
}

func removeCookiesDomain(domain: String) {
    for cookie in NSHTTPCookieStorage.sharedHTTPCookieStorage().cookies! {
        if cookie.domain.containsString(domain) {
            NSHTTPCookieStorage.sharedHTTPCookieStorage().deleteCookie(cookie)
        }
    }
}

func timeFromDate(timeIntervalSinceNow: Double?) -> String {
    guard timeIntervalSinceNow != nil else { return "" }
    let date = NSDate(timeIntervalSinceNow: timeIntervalSinceNow!)
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "HH:mm"
    return dateFormatter.stringFromDate(date)
}

func dateFromDate(timeIntervalSinceNow: Double?) -> String {
    guard timeIntervalSinceNow != nil else { return "" }
    let date = NSDate(timeIntervalSinceNow: timeIntervalSinceNow!)
    if NSCalendar.currentCalendar().isDateInToday(date) {
        return "Сегодня"
    } else if NSCalendar.currentCalendar().isDateInYesterday(date) {
        return "Вчера"
    } else {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd.MM.yy"
        return dateFormatter.stringFromDate(date)
    }
}

class MyImageCache {
    
    static let sharedCache: NSCache = {
        let cache = NSCache()
        cache.name = "MyImageCache"
        cache.countLimit = 20 // Max 20 images in memory.
        cache.totalCostLimit = 10*1024*1024 // Max 10MB used.
        return cache
    }()
    
}

extension NSURL {
    
    typealias ImageCacheCompletion = UIImage -> Void
    
    /// Retrieves a pre-cached image, or nil if it isn't cached.
    /// You should call this before calling fetchImage.
    var cachedImage: UIImage? {
        return MyImageCache.sharedCache.objectForKey(
            absoluteString) as? UIImage
    }
    
    /// Fetches the image from the network.
    /// Stores it in the cache if successful.
    /// Only calls completion on successful image download.
    /// Completion is called on the main thread.
    func fetchImage(completion: ImageCacheCompletion) {
        let task = NSURLSession.sharedSession().dataTaskWithURL(self) {
            data, response, error in
            if error == nil {
                if let  data = data,
                    image = UIImage(data: data) {
                    MyImageCache.sharedCache.setObject(
                        image,
                        forKey: self.absoluteString,
                        cost: data.length)
                    dispatch_async(dispatch_get_main_queue()) {
                        completion(image)
                    }
                }
            }
        }
        task.resume()
    }
    
}