//
//  LikesInfoEntity.swift
//  vktest
//
//  Created by developer2 on 13.08.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import Foundation
import CoreData


class LikesInfoEntity: NSManagedObject {

    var count: NSNumber?
    var user_likes: NSNumber?
    var can_like: NSNumber?
    var can_publish: NSNumber?

}
